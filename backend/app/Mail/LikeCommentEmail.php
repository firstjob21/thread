<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class LikeCommentEmail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    private $status;

    public function __construct(string $status)
    {
        $this->status = $status;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Comment like/dislike!')
            ->view('emails.like-comment', [
                'status' => $this->status,
            ]);
    }
}
