<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Like;

final class LikeRepository
{
    public function save(Like $like): Like
    {
        $like->save();

        return $like;
    }

    public function existsByUser(int $id, int $userId, string $class): bool
    {
        return Like::where([
            'likeable_id' => $id,
            'likeable_type' => $class,
            'user_id' => $userId
        ])->exists();
    }

    public function deleteByUser(int $id, int $userId, string $class): void
    {
        Like::where([
            'likeable_id' => $id,
            'likeable_type' => $class,
            'user_id' => $userId
        ])->delete();
    }
}
