<?php

declare(strict_types=1);

namespace App\Action\Tweet;

use App\Entity\Tweet;
use App\Exceptions\TweetNotFoundException;
use App\Exceptions\UserNotFoundException;
use App\Mail\LikeTweetEmail;
use App\Repository\TweetRepository;
use App\Repository\UserRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Mail\Mailer;

final class LikeTweetNotificationAction
{
    private $tweetRepository;
    private $userRepository;
    private $mailer;

    public function __construct(
        TweetRepository $tweetRepository,
        UserRepository $userRepository,
        Mailer $mailer
    ) {
        $this->tweetRepository = $tweetRepository;
        $this->userRepository = $userRepository;
        $this->mailer = $mailer;
    }

    public function execute(LikeTweetRequest $request, LikeTweetResponse $response): void
    {
        try {
            $tweet = $this->tweetRepository->getById($request->getTweetId());
            $recipient = $this->userRepository->getById($tweet->getAuthorId());
        } catch (ModelNotFoundException $ex) {
            if ($ex->getModel() === Tweet::class) {
                throw new TweetNotFoundException();
            }
            throw new UserNotFoundException();
        }

        $this->mailer->to($recipient)->send(new LikeTweetEmail($response->getStatus()));
    }
}
