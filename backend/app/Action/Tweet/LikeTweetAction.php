<?php

declare(strict_types=1);

namespace App\Action\Tweet;

use App\Entity\Like;
use App\Entity\Tweet;
use App\Repository\LikeRepository;
use App\Repository\TweetRepository;
use Illuminate\Support\Facades\Auth;

final class LikeTweetAction
{
    private $tweetRepository;
    private $likeRepository;

    private const ADD_LIKE_STATUS = 'added';
    private const REMOVE_LIKE_STATUS = 'removed';

    public function __construct(TweetRepository $tweetRepository, LikeRepository $likeRepository)
    {
        $this->tweetRepository = $tweetRepository;
        $this->likeRepository = $likeRepository;
    }

    public function execute(LikeTweetRequest $request): LikeTweetResponse
    {
        $tweet = $this->tweetRepository->getById($request->getTweetId());

        $userId = Auth::id();

        // if user already liked tweet, we remove previous like
        if ($this->likeRepository->existsByUser($tweet->id, $userId, Tweet::class)) {
            $this->likeRepository->deleteByUser($tweet->id, $userId, Tweet::class);

            return new LikeTweetResponse(self::REMOVE_LIKE_STATUS);
        }

        $like = new Like();
        $like->forEntity($userId, $tweet->id, Tweet::class);

        $this->likeRepository->save($like);

        return new LikeTweetResponse(self::ADD_LIKE_STATUS);
    }
}
