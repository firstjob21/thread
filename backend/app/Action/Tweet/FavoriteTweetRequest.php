<?php

declare(strict_types=1);

namespace App\Action\Tweet;

final class FavoriteTweetRequest
{
    private $userId;
    private $page;

    public function __construct(int $userId, int $page)
    {
        $this->userId = $userId;
        $this->page = $page;
    }

    public function getUserId(): int
    {
        return $this->userId;
    }

    public function getPage(): int
    {
        return $this->page;
    }
}
