<?php

declare(strict_types=1);

namespace App\Action\Tweet;

use App\Action\PaginatedResponse;
use App\Repository\TweetRepository;

final class GetFavoriteTweetCollectionByUserIdAction
{
    private $tweetRepository;

    public function __construct(TweetRepository $tweetRepository)
    {
        $this->tweetRepository = $tweetRepository;
    }

    public function execute(FavoriteTweetRequest $request): PaginatedResponse
    {
        return new PaginatedResponse(
            $this->tweetRepository->getFavoriteByUserId($request->getUserId(), $request->getPage())
        );
    }
}
