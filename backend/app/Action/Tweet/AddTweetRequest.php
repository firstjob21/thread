<?php

declare(strict_types=1);

namespace App\Action\Tweet;

final class AddTweetRequest
{
    private $text;
    private $hash;

    public function __construct(string $text, string $hash)
    {
        $this->text = $text;
        $this->hash = $hash;
    }

    public function getText(): string
    {
        return $this->text;
    }

    public function getHash(): string
    {
        return $this->hash;
    }
}
