<?php

declare(strict_types=1);

namespace App\Action\Comment;

use App\Entity\Comment;
use App\Entity\Like;
use App\Entity\Tweet;
use App\Mail\WelcomeEmail;
use App\Repository\LikeRepository;
use App\Repository\CommentRepository;
use Illuminate\Support\Facades\Auth;

final class LikeCommentAction
{
    private $commentRepository;
    private $likeRepository;

    private const ADD_LIKE_STATUS = 'added';
    private const REMOVE_LIKE_STATUS = 'removed';

    public function __construct(CommentRepository $commentRepository, LikeRepository $likeRepository)
    {
        $this->commentRepository = $commentRepository;
        $this->likeRepository = $likeRepository;
    }

    public function execute(LikeCommentRequest $request): LikeCommentResponse
    {
        $comment = $this->commentRepository->getById($request->getCommentId());

        $userId = Auth::id();

        // if user already liked comment, we remove previous like
        if ($this->likeRepository->existsByUser($comment->id, $userId, Comment::class)) {
            $this->likeRepository->deleteByUser($comment->id, $userId, Comment::class);

            return new LikeCommentResponse(self::REMOVE_LIKE_STATUS);
        }

        $like = new Like();
        $like->forEntity($userId, $comment->id, Comment::class);

        $this->likeRepository->save($like);

        return new LikeCommentResponse(self::ADD_LIKE_STATUS);
    }
}
