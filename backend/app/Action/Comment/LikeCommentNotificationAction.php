<?php

declare(strict_types=1);

namespace App\Action\Comment;

use App\Entity\Comment;
use App\Exceptions\CommentNotFoundException;
use App\Exceptions\UserNotFoundException;
use App\Mail\LikeCommentEmail;
use App\Repository\CommentRepository;
use App\Repository\UserRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Mail\Mailer;

final class LikeCommentNotificationAction
{
    private $commentRepository;
    private $userRepository;
    private $mailer;

    public function __construct(
        CommentRepository $commentRepository,
        UserRepository $userRepository,
        Mailer $mailer
    ) {
        $this->commentRepository = $commentRepository;
        $this->userRepository = $userRepository;
        $this->mailer = $mailer;
    }

    public function execute(LikeCommentRequest $request, LikeCommentResponse $response): void
    {
        try {
            $comment = $this->commentRepository->getById($request->getCommentId());
            $recipient = $this->userRepository->getById($comment->getAuthorId());
        } catch (ModelNotFoundException $ex) {
            if ($ex->getModel() === Comment::class) {
                throw new CommentNotFoundException();
            }
            throw new UserNotFoundException();
        }

        $this->mailer->to($recipient)->send(new LikeCommentEmail($response->getStatus()));
    }
}
