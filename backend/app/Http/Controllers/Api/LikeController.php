<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api;

use App\Action\Comment\LikeCommentAction;
use App\Action\Comment\LikeCommentNotificationAction;
use App\Action\Comment\LikeCommentRequest;
use App\Action\Tweet\LikeTweetAction;
use App\Action\Tweet\LikeTweetNotificationAction;
use App\Action\Tweet\LikeTweetRequest;
use App\Http\Controllers\ApiController;
use App\Http\Response\ApiResponse;

final class LikeController extends ApiController
{
    private $likeTweetAction;
    private $likeCommentAction;
    private $likeCommentNotificationAction;
    private $likeTweetNotificationAction;

    public function __construct(
        LikeTweetAction $likeTweetAction,
        LikeCommentAction $likeCommentAction,
        LikeCommentNotificationAction $likeCommentNotificationAction,
        LikeTweetNotificationAction $likeTweetNotificationAction
    ) {
        $this->likeTweetAction = $likeTweetAction;
        $this->likeCommentAction = $likeCommentAction;
        $this->likeCommentNotificationAction = $likeCommentNotificationAction;
        $this->likeTweetNotificationAction = $likeTweetNotificationAction;
    }

    public function likeOrDislikeTweet(string $id): ApiResponse
    {
        $likeTweetRequest = new LikeTweetRequest((int)$id);

        $response = $this->likeTweetAction->execute($likeTweetRequest);

        $this->likeTweetNotificationAction->execute($likeTweetRequest, $response);

        return $this->createSuccessResponse(['status' => $response->getStatus()]);
    }

    public function likeOrDislikeComment(string $id): ApiResponse
    {
        $likeCommentRequest = new LikeCommentRequest((int)$id);

        $response = $this->likeCommentAction->execute($likeCommentRequest);

        $this->likeCommentNotificationAction->execute($likeCommentRequest, $response);

        return $this->createSuccessResponse(['status' => $response->getStatus()]);
    }
}
