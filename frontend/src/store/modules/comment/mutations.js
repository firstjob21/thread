import { commentMapper } from '@/services/Normalizer';
import {
    SET_COMMENTS,
    ADD_COMMENT,
    DELETE_COMMENT,
    LIKE_COMMENT,
    DISLIKE_COMMENT,
    SET_COMMENT_IMAGE,
    SET_COMMENT
} from './mutationTypes';

export default {
    [SET_COMMENTS]: (state, comments) => {
        state.comments = {
            ...state.comments,
            ...comments.reduce(
                (prev, comment) => ({ ...prev, [comment.id]: commentMapper(comment) }),
                {}
            ),
        };
    },

    [ADD_COMMENT]: (state, comment) => {
        state.comments = {
            ...state.comments,
            [comment.id]: commentMapper(comment)
        };
    },

    [SET_COMMENT_IMAGE]: (state, { id, imageUrl }) => {
        state.comments[id].imageUrl = imageUrl;
    },

    [SET_COMMENT]: (state, comment) => {
        state.comments = {
            ...state.comments,
            [comment.id]: commentMapper(comment)
        };
    },

    [DELETE_COMMENT]: (state, id) => {
        delete state.comments[id];
    },

    [LIKE_COMMENT]: (state, { id, userId }) => {
        state.comments[id].likesCount++;

        state.comments[id].likes.push({ userId });
    },

    [DISLIKE_COMMENT]: (state, { id, userId }) => {
        state.comments[id].likesCount--;

        state.comments[id].likes = state.comments[id].likes.filter(like => like.userId !== userId);
    }
};
